variable "resource_group_name" {
  description = "The name of the resource group"
  default     = "aks-case-cluster"
}

variable "location" {
  description = "The Azure region to deploy the resources"
  default     = "germanywestcentral"
}

variable "vm_name" {
  description = "The name of the virtual machine"
  default     = "bastion-machine-for-aks"
}

variable "vm_nic" {
  description = "The name of the virtual machine"
  default     = "bastion-machine-nic"
}

variable "vm_size_system" {
  description = "The size of the virtual machine"
  default     = "Standard_B2s"
}

variable "vm_size_user" {
  description = "The size of the virtual machine"
  default     = "Standard_B4ms"
}