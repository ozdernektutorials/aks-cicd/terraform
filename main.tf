# Resource Group
resource "azurerm_resource_group" "aks_rg" {
  name     = var.resource_group_name
  location = var.location
}


# Virtual Network
resource "azurerm_virtual_network" "aks_vnet" {
  name                = "aks-vnet"
  address_space       = ["10.0.0.0/8"]
  location            = azurerm_resource_group.aks_rg.location
  resource_group_name = azurerm_resource_group.aks_rg.name
}

# Subnet
resource "azurerm_subnet" "aks_subnet" {
  name                 = "aks-subnet"
  resource_group_name  = azurerm_resource_group.aks_rg.name
  virtual_network_name = azurerm_virtual_network.aks_vnet.name
  address_prefixes     = ["10.240.0.0/16"]
}

# Public IP for bastion VM
resource "azurerm_public_ip" "bastion_machine_ip" {
  name                = "bastion-machine-public-ip"
  location            = azurerm_resource_group.aks_rg.location
  resource_group_name = azurerm_resource_group.aks_rg.name
  allocation_method   = "Dynamic"
}


# AKS Cluster
resource "azurerm_kubernetes_cluster" "aks_cluster" {
  name                    = "aks-cluster"
  location                = azurerm_resource_group.aks_rg.location
  resource_group_name     = azurerm_resource_group.aks_rg.name
  private_cluster_enabled = true
  dns_prefix              = "aks-cluster"
  kubernetes_version      = 1.27


  default_node_pool {
    name                = "default"
    enable_auto_scaling = true
    min_count           = 2
    max_count           = 5
    node_count          = 2
    vm_size             = var.vm_size_system
    zones               = ["1", "2", "3"]
    os_disk_size_gb     = 30
    vnet_subnet_id      = azurerm_subnet.aks_subnet.id

  }

  identity {
    type = "SystemAssigned"
  }

  network_profile {
    network_plugin    = "azure"
    network_policy    = "calico"
    load_balancer_sku = "standard"
  }

  tags = {
    Environment = "development"
  }
}

# User Node Pool
resource "azurerm_kubernetes_cluster_node_pool" "user_pool" {
  name                  = "userpool"
  kubernetes_cluster_id = azurerm_kubernetes_cluster.aks_cluster.id
  vm_size               = var.vm_size_user
  enable_auto_scaling   = true
  min_count             = 2
  max_count             = 5
  node_count            = 2
  zones                 = ["1", "2", "3"]
  os_disk_size_gb       = 30
  vnet_subnet_id        = azurerm_subnet.aks_subnet.id
}


# Create virtual machine to connect private aks cluster securely like bastion.check 

resource "azurerm_network_interface" "bastion_machine" {
  name                = var.vm_nic
  location            = azurerm_resource_group.aks_rg.location
  resource_group_name = azurerm_resource_group.aks_rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.aks_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.bastion_machine_ip.id

  }
}

resource "azurerm_linux_virtual_machine" "bastion_machine" {
  name                = var.vm_name
  resource_group_name = azurerm_resource_group.aks_rg.name
  location            = azurerm_resource_group.aks_rg.location
  size                = "Standard_B2s"
  admin_username      = "ozkan"
  network_interface_ids = [
    azurerm_network_interface.bastion_machine.id,
  ]

  admin_ssh_key {
    username   = "ozkan"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
}